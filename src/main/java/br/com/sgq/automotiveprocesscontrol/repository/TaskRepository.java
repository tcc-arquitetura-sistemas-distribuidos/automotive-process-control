package br.com.sgq.automotiveprocesscontrol.repository;

import br.com.sgq.automotiveprocesscontrol.model.TaskModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends CrudRepository<TaskModel, Long> {
}
