package br.com.sgq.automotiveprocesscontrol.repository;

import br.com.sgq.automotiveprocesscontrol.model.WorkflowModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkflowRepository extends CrudRepository<WorkflowModel, Long> {
}
