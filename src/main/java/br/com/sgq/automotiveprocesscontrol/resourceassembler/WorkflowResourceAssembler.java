package br.com.sgq.automotiveprocesscontrol.resourceassembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import br.com.sgq.automotiveprocesscontrol.controller.WorkflowController;
import br.com.sgq.automotiveprocesscontrol.model.WorkflowModel;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class WorkflowResourceAssembler implements ResourceAssembler<WorkflowModel, Resource<WorkflowModel>> {
  @Override
  public Resource<WorkflowModel> toResource(WorkflowModel workflow) {

    return new Resource<>(
        workflow,
        linkTo(methodOn(WorkflowController.class).byId(workflow.getId())).withSelfRel(),
        linkTo(methodOn(WorkflowController.class).all()).withRel("workflows")
    );
  }
}