package br.com.sgq.automotiveprocesscontrol.resourceassembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import br.com.sgq.automotiveprocesscontrol.controller.TaskController;
import br.com.sgq.automotiveprocesscontrol.model.TaskModel;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class TaskResourceAssembler implements ResourceAssembler<TaskModel, Resource<TaskModel>> {
  @Override
  public Resource<TaskModel> toResource(TaskModel task) {

    return new Resource<>(
        task,
        linkTo(methodOn(TaskController.class).byId(task.getId())).withSelfRel(),
        linkTo(methodOn(TaskController.class).all()).withRel("tasks")
    );
  }
}