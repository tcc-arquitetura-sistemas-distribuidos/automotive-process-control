package br.com.sgq.automotiveprocesscontrol.service.impl;

import br.com.sgq.automotiveprocesscontrol.model.TaskModel;
import br.com.sgq.automotiveprocesscontrol.model.WorkflowModel;
import br.com.sgq.automotiveprocesscontrol.queue.sender.WorkflowSender;
import br.com.sgq.automotiveprocesscontrol.repository.TaskRepository;
import br.com.sgq.automotiveprocesscontrol.repository.WorkflowRepository;
import br.com.sgq.automotiveprocesscontrol.service.WorkflowService;
import java.text.MessageFormat;
import java.util.function.Function;
import java.util.function.Supplier;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class WorkflowServiceImpl implements WorkflowService {

  private final WorkflowRepository workflowRepository;
  private final TaskRepository taskRepository;
  private final WorkflowSender workflowSender;

  public WorkflowServiceImpl(
      WorkflowRepository workflowRepository,
      TaskRepository taskRepository,
      WorkflowSender workflowSender) {
    this.workflowRepository = workflowRepository;
    this.taskRepository = taskRepository;
    this.workflowSender = workflowSender;
  }

  @Override
  public Iterable<WorkflowModel> all() {
    return workflowRepository.findAll();
  }

  @Override
  public WorkflowModel create(WorkflowModel workflow) {
    workflowRepository.save(workflow);

    workflowSender.sendWorkflowToQueue(workflow);

    return workflow;
  }

  @Override
  public WorkflowModel getById(Long workflowId) {
    return workflowRepository.findById(workflowId)
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public WorkflowModel updateById(Long id, WorkflowModel newWorkflow) {
    return workflowRepository.findById(id)
        .map(updateWorkflow(newWorkflow))
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public WorkflowModel delete(Long id) {
    return workflowRepository.findById(id)
        .map(this::deleteWorkflow)
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public WorkflowModel updateByIdWorkflowTask(Long workflowId, TaskModel task) {
    taskRepository.findById(task.getId())
        .orElseThrow(resourceNotFoundException("Task", task.getId()));

    return workflowRepository.findById(workflowId)
        .map(updateWorkflowTask(task))
        .orElseThrow(resourceNotFoundException("Workflow", workflowId));
  }

  private WorkflowModel deleteWorkflow(WorkflowModel workflow) {
    workflowRepository.delete(workflow);

    return workflow;
  }

  private Function<WorkflowModel, WorkflowModel> updateWorkflow(WorkflowModel newWorkflow) {
    return (workflow) -> {
      workflow.setDescription(newWorkflow.getDescription());
      workflow.setStartsAt(newWorkflow.getStartsAt());
      workflow.setEndsAt(newWorkflow.getEndsAt());

      return workflowRepository.save(workflow);
    };
  }

  private Function<WorkflowModel, WorkflowModel> updateWorkflowTask(TaskModel newTask) {
    return (workflow) -> {
      workflow.setTask(newTask);

      return workflowRepository.save(workflow);
    };
  }

  private Supplier resourceNotFoundException(String resource, Long id) {
    return () -> new ResourceNotFoundException(
        MessageFormat.format("{0} Resource with id {1} not found", resource, id)
    );
  }
}
