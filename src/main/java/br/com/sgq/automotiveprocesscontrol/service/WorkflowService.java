package br.com.sgq.automotiveprocesscontrol.service;

import br.com.sgq.automotiveprocesscontrol.model.TaskModel;
import br.com.sgq.automotiveprocesscontrol.model.WorkflowModel;

public interface WorkflowService {

  Iterable<WorkflowModel> all();

  WorkflowModel create(WorkflowModel workFlow);

  WorkflowModel getById(Long workflowId);

  WorkflowModel updateById(Long id, WorkflowModel newWorkflow);

  WorkflowModel delete(Long id);

  WorkflowModel updateByIdWorkflowTask(Long workflowId, TaskModel task);
}
