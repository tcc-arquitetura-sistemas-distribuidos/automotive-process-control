package br.com.sgq.automotiveprocesscontrol.service;

import br.com.sgq.automotiveprocesscontrol.model.TaskModel;

public interface TaskService {

  Iterable<TaskModel> all();

  TaskModel create(TaskModel task);

  TaskModel getById(Long taskId);

  TaskModel updateById(Long id, TaskModel task);

  TaskModel delete(Long id);
}
