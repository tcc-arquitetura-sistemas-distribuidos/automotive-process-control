package br.com.sgq.automotiveprocesscontrol.service.impl;

import br.com.sgq.automotiveprocesscontrol.model.TaskModel;
import br.com.sgq.automotiveprocesscontrol.repository.TaskRepository;
import br.com.sgq.automotiveprocesscontrol.service.TaskService;
import java.util.function.Function;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {

  private final TaskRepository taskRepository;

  public TaskServiceImpl(
      TaskRepository taskRepository) {
    this.taskRepository = taskRepository;
  }

  @Override
  public Iterable<TaskModel> all() {
    return taskRepository.findAll();
  }

  @Override
  public TaskModel create(TaskModel task) {
    taskRepository.save(task);
    return task;
  }

  @Override
  public TaskModel getById(Long taskId) {
    return taskRepository.findById(taskId)
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public TaskModel updateById(Long id, TaskModel newTask) {
    return taskRepository.findById(id)
        .map(updateTask(newTask))
        .orElseThrow(ResourceNotFoundException::new);
  }

  @Override
  public TaskModel delete(Long id) {
    return taskRepository.findById(id)
        .map(this::deleteTask)
        .orElseThrow(ResourceNotFoundException::new);
  }

  private TaskModel deleteTask(TaskModel task) {
    taskRepository.delete(task);

    return task;
  }

  private Function<TaskModel, TaskModel> updateTask(TaskModel newTask) {
    return (task) -> {
      task.setName(newTask.getName());
      task.setDescription(newTask.getDescription());

      return taskRepository.save(task);
    };
  }


}
