package br.com.sgq.automotiveprocesscontrol.controller;

import br.com.sgq.automotiveprocesscontrol.model.TaskModel;
import br.com.sgq.automotiveprocesscontrol.resourceassembler.TaskResourceAssembler;
import br.com.sgq.automotiveprocesscontrol.service.TaskService;
import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/tasks")
public class TaskController {

  private final TaskService taskService;
  private final TaskResourceAssembler assembler;

  public TaskController(TaskService taskService,
      TaskResourceAssembler assembler) {
    this.taskService = taskService;
    this.assembler = assembler;
  }

  @GetMapping
  public ResponseEntity all() {
    return ResponseEntity.ok(taskService.all());
  }

  @PostMapping
  public ResponseEntity create(@RequestBody TaskModel task) throws URISyntaxException {
    TaskModel createdTask = taskService.create(task);

    Resource<TaskModel> resource = assembler.toResource(createdTask);

    return ResponseEntity.created(new URI(resource.getId().expand().getHref()))
      .body(createdTask);
  }

  @GetMapping(path = "/{id}")
  public ResponseEntity byId(@PathVariable Long id) {
    return ResponseEntity.ok(taskService.getById(id));
  }

  @PutMapping(path = "/{id}")
  public ResponseEntity updateById(@PathVariable Long id, @RequestBody TaskModel task) {
    return ResponseEntity.ok(taskService.updateById(id, task));
  }

  @DeleteMapping(path = "/{id}")
  public ResponseEntity delete(@PathVariable Long id) {
    taskService.delete(id);

    return ResponseEntity.noContent().build();
  }

}
