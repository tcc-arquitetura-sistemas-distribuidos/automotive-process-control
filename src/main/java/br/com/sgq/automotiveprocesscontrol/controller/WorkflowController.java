package br.com.sgq.automotiveprocesscontrol.controller;

import br.com.sgq.automotiveprocesscontrol.model.TaskModel;
import br.com.sgq.automotiveprocesscontrol.model.WorkflowModel;
import br.com.sgq.automotiveprocesscontrol.resourceassembler.WorkflowResourceAssembler;
import br.com.sgq.automotiveprocesscontrol.service.WorkflowService;
import java.net.URI;
import java.net.URISyntaxException;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/workflows")
public class WorkflowController {

  private final WorkflowService workflowService;
  private final WorkflowResourceAssembler assembler;

  public WorkflowController(
      WorkflowService workflowService,
      WorkflowResourceAssembler assembler
  ) {
    this.workflowService = workflowService;
    this.assembler = assembler;
  }

  @GetMapping
  public ResponseEntity all() {
    return ResponseEntity.ok(workflowService.all());
  }

  @PostMapping
  public ResponseEntity create(@RequestBody WorkflowModel workflow) throws URISyntaxException {
    WorkflowModel createdWorkflow = workflowService.create(workflow);

    Resource<WorkflowModel> resource = assembler.toResource(createdWorkflow);

    return ResponseEntity.created(new URI(resource.getId().expand().getHref()))
        .body(createdWorkflow);
  }

  @GetMapping(path = "/{id}")
  public ResponseEntity byId(@PathVariable Long id) {
    return ResponseEntity.ok(workflowService.getById(id));
  }

  @PutMapping(path = "/{id}")
  public ResponseEntity updateById(@PathVariable Long id, @RequestBody WorkflowModel workflow) {
    return ResponseEntity.ok(workflowService.updateById(id, workflow));
  }

  @DeleteMapping(path = "/{id}")
  public ResponseEntity delete(@PathVariable Long id) {
    workflowService.delete(id);

    return ResponseEntity.noContent().build();
  }

  @PutMapping(path = "/{workflowId}/task")
  public ResponseEntity updateTask(@PathVariable Long workflowId, @RequestBody TaskModel task) {
    return ResponseEntity.ok(workflowService.updateByIdWorkflowTask(workflowId, task));
  }

}
