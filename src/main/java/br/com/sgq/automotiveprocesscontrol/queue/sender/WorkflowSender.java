package br.com.sgq.automotiveprocesscontrol.queue.sender;

import br.com.sgq.automotiveprocesscontrol.model.WorkflowModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class WorkflowSender {

  private final RabbitTemplate rabbitTemplate;
  private final String workflowQueue;

  public WorkflowSender(RabbitTemplate rabbitTemplate, @Value("${queue.workflow}") String workflowQueue) {
    this.rabbitTemplate = rabbitTemplate;
    this.workflowQueue = workflowQueue;
  }

  public void sendWorkflowToQueue(WorkflowModel workflow) {
    this.rabbitTemplate.convertAndSend(this.workflowQueue, workflow);
    log.info("Sent workflow {} to queue.", workflow.getId());
  }
}
