drop table if exists workflows;
drop table if exists tasks;

CREATE TABLE tasks (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(1000) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE workflows (
  id INT NOT NULL AUTO_INCREMENT,
  description VARCHAR(1000) NOT NULL,
  starts_at DATETIME NOT NULL,
  ends_at DATETIME NOT NULL,
  task_id INT,
  PRIMARY KEY (id),
  FOREIGN KEY (task_id) REFERENCES tasks(id)
);