# Módulo - Automotive Proccess Control

Para iniciar este microserviço junto com suas dependências execute os seguintes comandos:

```bash
docker-compose -f src/main/resources/docker/docker-compose.yml up -d
source environments.sh
mvn clean install
java -jar target/*.jar
```

## Swagger:
Após ter executado o docker-compose acima o serviço swagger ui estará disponível neste endereço:

http://localhost:8080

---

Utilizando a ferramenta [httpie](https://httpie.org/) é possível efetuar testes nas apis rest disponíveis nesta aplicação:

```
# Listar workflows:
http get :8090/automotive-proccess-control/v1/workflows

# Listar tasks:
http get :8090/automotive-proccess-control/v1/tasks

# Criar uma task:
http post :8090/automotive-proccess-control/v1/tasks < src/main/resources/mock-data/task.json

# Criar um workflow:
http post :8090/automotive-proccess-control/v1/workflows < src/main/resources/mock-data/workflow.json

# Registrar a task de um workflow:
http put :8090/automotive-proccess-control/v1/workflows/2/task id=1

# Entre outras chamadas disponíveis no swagger
```
